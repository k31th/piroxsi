(function($) {
  window.piroxsi = {
    config: null,
  
    setSpanMessage: function(id, message, color) {
      var lastMessage = $('<span />').attr('id', id)
                                     .html(message);
      
      if (color) {
        $(lastMessage).css('color', color);
      }
      
      $('#'+id).replaceWith(lastMessage);
    },
    
    setModalMessage: function(message, color) {
      window.piroxsi.setSpanMessage('lblModalMessage', message, color);
    },
    
    toggleModal: function() {
      $('#modalRefresh').modal('toggle');
    },
    
    toggleDisconnectModal: function() {
      $('#modalDisconnect').modal('toggle');
    }
  };
  
  $('#lnkRefreshResources').on('click', function() {
    window.piroxsi.setModalMessage('Refreshing resource list.  Please wait.');
    $('#modalRefresh').modal('show');

    $.get('/refresh', function(data, status) {
      if (eval(data.command)) {
        window.location = '/';
      } else {
        window.piroxsi.setModalMessage(data.args.error, 'red');        
      }
    }); 
  });
  
  $('#lnkUpdatePiroxsi').on('click', function() {
    $('#modalUpdate').modal('toggle');
  });
  
  $('#lnkDisconnectAll').on('click', function() {
    window.piroxsi.toggleDisconnectModal();
  });
  
  $('#lnkModalUpdate').on('click', function() {
    window.piroxsi.setModalMessage('Updating Piroxsi. Please wait.');
    $('#modalRefresh').modal('show');
    
    $.get('/updatepiroxsi', function(data, status) {
      if (data.command === 'updatepiroxsi') {
        if ( data.args.success ) {
          window.location = '/';
        } else {
          window.piroxsi.setModalMessage('Error Updating', 'red');
        }      
      } else {
        window.piroxsi.setModalMessage(data.args.error, 'red');
      }
    });
  });
  
  $('#lnkModalDisconnectAll').on('click', function() {
    $('#modalDisconnect').modal('hide');
    
    window.piroxsi.setModalMessage('Disconnecting all connections. Please wait.');
    $('#modalRefresh').modal('show');
    
    $.get('/disconnectall', function(data, status) {
      if (data.command === 'disconnectall') {
        if ( data.args.success ) {
          window.location = '/';
        } else {
          window.piroxsi.setModalMessage('Error Disconnecting', 'red');
        }      
      } else {
        window.piroxsi.setModalMessage(data.args.error, 'red');
      }
    });
  });
})(jQuery);