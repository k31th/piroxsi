<!DOCTYPE html> 
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Piroxsi</title>
    <link href="{{ url('assets/style/bootstrap.min.css') }}" type="text/css" rel="stylesheet" />
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Piroxsi</a>&nbsp;&nbsp;
          
          @if ($isconnected)
            <a href="/status/{{ $connectedid }}">
              <span id="lblIsConnected" class="navbar-header label label-success" style="margin-top: 18px;">Connected</span>
            </a>
          @else
            <span id="lblIsConnected" class="navbar-header label label-danger" style="margin-top: 18px;">Disconnected</span>
          @endif
          
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            
            @if ($isconnected) 
              <li><a id="lnkDisconnectAll" href="#">Disconnect All</a></li>   
            @else
              <li><a id="lnkDisconnectAll" href="#" style="display:none;">Disconnect All</a></li>
            @endif
            
            <li><a id="lnkRefreshResources" href="#">Refresh Resources</a></li>
            <li><a id="lnkUpdatePiroxsi" href="#">Update Piroxsi</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container">
      <div class="row">
        <div class="col-md-12"><br/><br/><br /></div>
      </div>
    </div>
    
    @yield('content')

    <div id="modalRefresh" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalRefreshLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalRefreshLabel">Piroxsi</h5>
          </div>
          <div class="modal-body">
            <p id="lblModalMessage">We are now refreshing the resource list</p>
          </div>
          <div class="modal-footer"></div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="modalDisconnectLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalUpdateLabel">Piroxsi</h5>
          </div>
          <div class="modal-body">
            <p id="lblUpdateMessage">Are you sure you want to update the application?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <a class="btn btn-danger btn-ok" id="lnkModalUpdate">Update</a>
          </div>
        </div>
      </div>
    </div>    
    
    <div class="modal fade" id="modalDisconnect" tabindex="-1" role="dialog" aria-labelledby="modalDisconnectLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalDisconnectLabel">Piroxsi</h5>
          </div>
          <div class="modal-body">
            <p id="lblDisconnectMessage">You are currently connected to a resource.  Do you want to disconnect?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <a class="btn btn-danger btn-ok" id="lnkModalDisconnectAll">Disconnect</a>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="{{ url('assets/script/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/script/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/script/piroxsi.js') }}"></script>
    <script type="text/javascript">window.piroxsi.config={!! $config !!};</script>
    @stack('scripts')
  </body>
</html>
