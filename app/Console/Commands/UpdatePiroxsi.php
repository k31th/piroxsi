<?php
  namespace App\Console\Commands;
  
  use Illuminate\Console\Command;

  class UpdatePiroxsi extends Command {
    protected $name = 'updatepiroxsi';    
    protected $description = 'Gets the latest version of Piroxsi from GitLab';
    
    public function fire() {      
      $basePath =  base_path();
      
      exec('cd ' . $basePath . ' && resources/scripts/updatePiroxsi');
    }
  }

